function util.ClipPunchAngleOffset(angIn, angPunch, angClip)
	if not (angIn or isangle(angIn)) or not (angPunch or isangle(angPunch)) or not (angClip or not isangle(angClip)) then return end
	local final = angIn + angPunch

	for i = 1,3 do
		if final[i] > angClip[i] then
			final[i] = angClip[i]
		elseif final[i] < -angClip[i] then
			final[i] = -angClip[i]
		end
		final[i] = final[i] - angPunch[i]
	end

	-- cpp version sets angIn's x,y,z
	return final
end

if not util.ImpactTrace then
	function util.ImpactTrace(tr, iDamageType, effect)
		if not tr.Entity or tr.HitSky then
			return end
		if tr.Fraction == 1 then
			return end
		if tr.HitNoDraw then
			return end

		local data = EffectData()
		data:SetOrigin(tr.HitPos)
		data:SetStart(tr.StartPos)
		data:SetSurfaceProp(tr.SurfaceProps)
		data:SetDamageType(iDamageType)
		data:SetHitBox(tr.HitBox)
		data:SetEntity(tr.Entity)

		if SERVER or (CLIENT and IsFirstTimePredicted()) then
			util.Effect(effect or "Impact", data, not game.SinglePlayer())
		end
	end
end

local FX_WATER_IN_SLIME = 0x1
function util.BulletImpact(tr, ply)
	-- see if the bullet ended up underwater and started out of the water
	if bit.band(util.PointContents(tr.HitPos), bit.bor(CONTENTS_WATER, CONTENTS_SLIME)) ~= 0 then
		local waterTrace = {}
		util.TraceLine({
			start = tr.StartPos,
			endpos = tr.HitPos,
			mask = bit.bor(CONTENTS_WATER,CONTENTS_SLIME),
			filter = ply,
			collisiongroup = COLLISION_GROUP_NONE,
			output = waterTrace,
		})

		if not waterTrace.AllSolid then
			local data = EffectData()
			data:SetOrigin(waterTrace.HitPos)
			data:SetNormal(waterTrace.HitNormal)
			data:SetScale(UniformRandomStream():RandomFloat(8, 12))

			if bit.band(waterTrace.Contents, CONTENTS_SLIME) ~= 0 then
				data:SetFlags(bit.bor(data:GetFlags(), FX_WATER_IN_SLIME))
			else
				data:SetFlags(bit.bnot(FX_WATER_IN_SLIME))
			end

			if SERVER and IsValid(ply) and ply:IsPlayer() then
				SuppressHostEvents(ply)
			end

			if SERVER or (CLIENT and IsFirstTimePredicted()) then
				util.Effect("gunshotsplash", data, not game.SinglePlayer())
			end

			return
		end
	end

	if SERVER and IsValid(ply) and ply:IsPlayer() then
		SuppressHostEvents(ply)
	end

	util.ImpactTrace(tr, DMG_BULLET)
end

function util.CalculateMeleeDamageForce(info, vecMeleeDir, vecForceOrigin, flScale)
	info:SetDamagePosition(vecForceOrigin)

	-- Calculate an impulse large enough to push a 75kg man 4 in/sec per point of damage
	local flForceScale = info:GetBaseDamage() * (75 * 4)
	local vecForce = vecMeleeDir
	vecForce:Normalize()
	vecForce = vecForce * flForceScale
	vecForce = vecForce * GetConVar("phys_pushscale"):GetFloat()
	vecForce = vecForce * flScale
	info:SetDamageForce(vecForce)
end

function util.CreateCombineBall(vecOrigin, vecVel, flRadius, flMass, flLifeTime, entOwner)
	local ball = ents.Create("prop_combine_ball")
	ball:SetSaveValue("m_flRadius", flRadius)

	ball:SetPos(vecOrigin)
	ball:SetOwner(entOwner)

	ball:SetVelocity(vecVel)
	ball:Spawn()

	ball:SetSaveValue("m_nState", 2)
	ball:SetSaveValue("m_flSpeed", vecVel:Length())

	ball:EmitSound("NPC_CombineBall.Launch")

	local phys = ball:GetPhysicsObject()
	if IsValid(phys) then
		phys:AddGameFlag(FVPHYSICS_WAS_THROWN)
		phys:SetMass(flMass)
		phys:SetVelocity(vecVel)
	end

	ball:SetSaveValue("m_bWeaponLaunched", true)
	ball:SetSaveValue("m_bLaunched", true)

	-- isnt the best but oh well
	timer.Simple(flLifeTime, function()
		if IsValid(ball) and ball:GetInternalVariable("m_bHeld") == false then
			ball:Fire("Explode")
		end
	end)

	return ball
end

local function fsel(c, x, y)
	return c >= 0 and x or y
end
function util.RemapVal(val,a,b,c,d)
	if a == b then
		return fsel(val - b, d, c)
	end
	return c + (d - c) * (val - a) / (b - a)
end
function util.RemapValClamped(val,a,b,c,d)
	if a == b then
		return fsel(val - b, d, c)
	end

	local cVal = (val - a) / (b - a)
	cVal = math.Clamp(cVal, 0, 1)
	return c + (d - c) * cVal
end

local lastAmt = -1
local lastExponent = -1
function util.Bias(x, biasAmt)
	if lastAmt ~= biasAmt then
		lastExponent = math.log(biasAmt) * -1.4427 -- (-1.4427 = 1 / log(0.5))
	end

	return math.pow(x, lastExponent)
end

function util.Gain(x, biasAmt)
	if x < 0.5 then
		return 0.5 * util.Bias(2 * x, 1-biasAmt)
	else
		return 1 - 0.5 * util.Bias(2 - 2 * x, 1-biasAmt)
	end
end

-- hammer to irl measurements
do
	local UNIT_TO_INCH = .75
	local INCH_TO_CM = 2.54
	function util.HUtoInch(units)
		return units * UNIT_TO_INCH
	end
	function util.HUtoCM(units)
		return util.HUtoInch(units) * INCH_TO_CM
	end
	function util.NiceMetric(cm)
		local m = cm / 100
		return string.format("%.1f%s", m >= 1 and m or cm, m >= 1 and "m" or "cm")
	end
end

IN_ATTACK3 = bit.lshift(1, 25)

source_weps = source_weps or {}

local PLAYER = FindMetaTable "Player"

if CLIENT then
	local function ScreenScaleH(n)
		return n * (ScrH() / 480)
	end

	local fov_desired = GetConVar "fov_desired"
	function PLAYER:GetDefaultFOV()
		return fov_desired:GetInt()
	end

	surface.CreateFont("_sw_wepfont_0", {
		font = "HalfLife2",
		size = ScreenScaleH(64),
		weight = 0,
		antialias = true,
		additive = true,
		blursize = 0,
		scanlines = 0,
	})

	surface.CreateFont("_sw_wepfont_1", {
		font = "HalfLife2",
		size = ScreenScaleH(64),
		weight = 0,
		antialias = true,
		additive = true,
		blursize = ScreenScaleH(4),
		scanlines = ScreenScaleH(2),
	})
else
	function PLAYER:GetDefaultFOV()
		return self:GetInternalVariable "m_iDefaultFOV"
	end
end
